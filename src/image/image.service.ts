import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Image } from './image.entity';

@Injectable()
export class ImageService {
  constructor(
    @InjectRepository(Image)
    private readonly imageRepository: Repository<Image>,
  ) {}

  async findAll(): Promise<Image[]> {
    return await this.imageRepository.find();
  }

  async findOne(id) {
    return this.imageRepository.findByIds(id);
  }

  async create() {
    return this.imageRepository.create();
  }

  async delete(id) {
    return this.imageRepository.delete(id);
  }
}
