import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Advert } from '../advert/advert.entity';
import { InfoHome } from '../info-home/info-home.entity';

@Entity()
export class Image {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  url: string;

  @Column()
  sort: string;

  @Column()
  file: string;

  @Column()
  dow: boolean;

  @ManyToOne(type => Advert, advert => advert.images, {
    nullable: true,
  })
  @JoinColumn({name : 'productAdvert_id'})
  advert: Advert;

  @ManyToOne(type => InfoHome, infoHome => infoHome.images, {
    nullable: true,
  })
  @JoinColumn({name : 'productInfoHome_id'})
  infoHome: InfoHome;
}
