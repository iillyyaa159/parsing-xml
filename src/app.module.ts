import { Module } from '@nestjs/common';
import { AdvertModule } from './advert/advert.module';
import { ImageModule } from './image/image.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InfoHomeModule } from './info-home/info-home.module';
import { ParsingModule } from './parsing/parsing.module';

@Module({
  imports: [TypeOrmModule.forRoot(), AdvertModule, ImageModule, InfoHomeModule, ParsingModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
