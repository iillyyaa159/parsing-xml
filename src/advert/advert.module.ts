import { Module } from '@nestjs/common';
import { AdvertService } from './advert.service';
import { Advert } from './advert.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Advert])],
  controllers: [],
  providers: [AdvertService],
})
export class AdvertModule {}
