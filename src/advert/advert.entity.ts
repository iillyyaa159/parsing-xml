import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  ManyToOne,
} from 'typeorm';
import { Image } from '../image/image.entity';
import { InfoHome } from '../info-home/info-home.entity';

@Entity()
export class Advert {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  offer_id: number;

  @Column({ nullable: true })
  type: string;

  @Column({ nullable: true })
  category: string;

  @Column({ nullable: true })
  mortgage: string;

  @Column({ nullable: true })
  dealStatus: string;

  @Column({ nullable: true })
  locationLocalityName: string;

  @Column({ nullable: true })
  locationAddress: string;

  @Column({ type: 'float', nullable: true })
  locationLatitude: number;

  @Column({ type: 'float', nullable: true })
  locationLongitude: number;

  @Column({ nullable: true })
  locationApartment: number;

  @Column({ type: 'decimal', nullable: true })
  priceValue: number;

  @Column({ nullable: true })
  priceCurrency: string;

  @Column({ type: 'float', nullable: true })
  areaValue: number;

  @Column({ type: 'float', nullable: true })
  livingSpaceValue: number;

  @Column({ type: 'float', nullable: true })
  kitchenSpaceValue: number;

  @Column({ nullable: true })
  renovation: string;

  @Column({ type: 'text', nullable: true })
  description: string;

  @Column({ nullable: true })
  newFlat: number;

  @Column({ nullable: true })
  floor: number;

  @Column({ nullable: true })
  rooms: number;

  @Column({ nullable: true })
  floorsTotal: number;

  @Column()
  status: boolean;

  @Column({ nullable: true })
  main_photo_id: number;

  @OneToMany(type => Image, image => image.advert, {
    cascade: true,
  })
  images: Image[];

  @ManyToOne(type => InfoHome, infoHome => infoHome.adverts)
  infoHome: InfoHome;
}
