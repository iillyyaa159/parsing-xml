import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Advert } from './advert.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AdvertService {
  constructor(
    @InjectRepository(Advert)
    private readonly advertRepository: Repository<Advert>,
  ) {}

  async findAll(): Promise<Advert[]> {
    return await this.advertRepository.find();
  }

  async findOne(id) {
    return this.advertRepository.findByIds(id);
  }

  async create(advert) {
    return this.advertRepository.create(advert);
  }

  async delete(id) {
    return this.advertRepository.delete(id);
  }
}
