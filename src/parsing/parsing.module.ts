import { Module } from '@nestjs/common';
import { ParsingController } from './parsing.controller';
import { InfoHome } from '../info-home/info-home.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InfoHomeService } from '../info-home/info-home.service';
import { ParsingService } from './parsing.service';

@Module({
  controllers: [ParsingController],
  providers: [InfoHomeService, ParsingService],
  imports: [TypeOrmModule.forFeature([InfoHome])],
})
export class ParsingModule {}
