import { Controller } from '@nestjs/common';
import { InfoHomeService } from '../info-home/info-home.service';
import * as http from 'http';
import { getConnection } from 'typeorm';
import { Advert } from '../advert/advert.entity';
import { InfoHome } from '../info-home/info-home.entity';
import { Image } from '../image/image.entity';
import * as fs from 'fs';
import * as sleep from 'system-sleep';

@Controller('parsing')
export class ParsingController {
  constructor(
    private infoHomeService: InfoHomeService,
  ) {
    this.AutoStarting();
  }

  async AutoStarting() {
    while (true) {
      await this.Parsing();
    }
  }

  async Parsing() {
    try {
      const offets = await this.getDoc();

      await getConnection()
        .createQueryBuilder()
        .update(Advert)
        .set({ status: true })
        .execute();

      const infoHomes = [];

      for (const i in offets){
        let advert = this.parseAdvert(offets[i]);
        let infoHome = this.parseInfoHome(offets[i]);

        const advertGet = await getConnection()
          .getRepository(Advert)
          .createQueryBuilder('advert')
          .leftJoinAndSelect('advert.images', 'image')
          .where('offer_id = :offer_id', { offer_id: advert.offer_id })
          .getOne();

        if (advertGet !== undefined) {
          advert.id = advertGet.id;
          advert.images = advertGet.images;
          await getConnection().manager.save(advert);
        }
        else {
          infoHome = await this.CheckInfoHome(infoHome);

          if (infoHome.adverts === undefined){
            infoHome.adverts = [];
          }

          advert.infoHome = infoHome;
          advert = await getConnection().manager.save(advert);
          if (advert.images.length > 0){
            advert.main_photo_id = advert.images[0].id;
            await getConnection().manager.save(advert);
          }

          infoHome = await getConnection()
            .getRepository(InfoHome)
            .createQueryBuilder('infoHome')
            .leftJoinAndSelect('infoHome.images', 'images')
            .where(`infoHome.id = ${infoHome.id}`)
            .getOne();

          if (infoHome.images.length > 0){
            infoHome.main_photo_id = infoHome.images[0].id;
            await getConnection().manager.save(infoHome);
          }
        }
      }

      await this.SaveImage();
      console.log('Done, time = ' + new Date());
      sleep(60000 * 5);
    } catch (ex) {
      console.log(ex);
    }
  }

  async CheckInfoHome(infoHome){

    const infoHomeGet = await this.infoHomeService.findOne(infoHome);

    if (!infoHomeGet){
      return await getConnection().getRepository(InfoHome).save(infoHome);
    }

    return infoHomeGet;
  }

  async SaveImage() {
    (!fs.existsSync(`./images/`)) && (fs.mkdirSync(`./images/`));
    (!fs.existsSync(`./image_build/`)) && (fs.mkdirSync(`./image_build/`));

    let countImageSave = 0;

    while (true) {
      const image = await getConnection()
        .getRepository(Image)
        .createQueryBuilder('image')
        .leftJoinAndSelect('image.advert', 'advert')
        .leftJoinAndSelect('image.infoHome', 'infoHome')
        .where('image.dow = false')
        .getOne();

      if (image === undefined) break;

      let path = '';

      if (image.advert !== null) {
        path = `./images/${image.advert.id}`;
      }

      if (image.infoHome !== null) {
        path = `./image_build/${image.infoHome.id}`;
      }

      if (path === undefined) continue;

      (!fs.existsSync(path)) && (fs.mkdirSync(path));

      image.file = image.id + image.url.substring(image.url.lastIndexOf('.'));

      await new Promise(async end => {
        await http.get(image.url, async response => {
          const file = await fs.createWriteStream(path + `/${image.file}`);
          await response.pipe(file);
          image.dow = true;
          end();
        });
      });

      await getConnection()
        .createQueryBuilder()
        .update(Image)
        .set(image)
        .where('id = :id', { id: image.id })
        .execute();

      countImageSave++;
      console.log(countImageSave + ' фотографий скачано');
    }
  }

  parseAdvert(offer){
    const advert = new Advert();

    (offer.$['internal-id']) && (advert.offer_id = offer.$['internal-id']);
    advert.type = '1'; // offer.type[0])
    advert.category = '1'; // offer.category[0])
    (offer.mortgage[0]) && (advert.mortgage = offer.mortgage[0]);
    (offer['deal-status']) && (offer['deal-status'][0]) && (advert.dealStatus = offer['deal-status'][0]);
    advert.locationLocalityName = '77'; // offer.location[0]['locality-name'][0])
    (offer.location[0].address[0]) && (advert.locationAddress = offer.location[0].address[0]);
    (offer.location[0].latitude[0]) && (advert.locationLatitude = offer.location[0].latitude[0]);
    (offer.location[0].longitude[0]) && (advert.locationLongitude = offer.location[0].longitude[0]);
    (offer.location[0].apartment[0]) && (advert.locationApartment = offer.location[0].apartment[0]);
    (offer.price[0].value[0]) && (advert.priceValue = offer.price[0].value[0]);
    (offer.price[0].currency[0]) && (advert.priceCurrency = offer.price[0].currency[0]);
    (offer.area[0].value[0]) && (advert.areaValue = offer.area[0].value[0]);
    (offer['living-space']) && (offer['living-space'][0].value[0]) && (advert.livingSpaceValue = offer['living-space'][0].value[0]);
    (offer['kitchen-space']) && (offer['kitchen-space'][0].value[0]) && (advert.kitchenSpaceValue = offer['kitchen-space'][0].value[0]);
    (offer.renovation[0]) && (advert.renovation = offer.renovation[0]);
    (offer.description[0]) && (advert.description = offer.description[0]);
    (offer['new-flat'][0]) && (advert.newFlat = offer['new-flat'][0]);
    (offer.floor[0]) && (advert.floor = offer.floor[0]);
    (offer.rooms[0]) && (advert.rooms = offer.rooms[0]);
    (offer['floors-total'][0]) && (advert.floorsTotal = offer['floors-total'][0]);

    advert.status = false;

    advert.images = [];
    for (const image of offer.image) {
      if (typeof image === 'string') continue;

      const imageD = new Image();
      imageD.dow = false;
      imageD.file = '';

      // @ts-ignore
      imageD.url = image._.replace('\n\t\t\t\t\t', '').replace(
        '\n\t\t\t\t',
        '',
      );

      imageD.sort = imageD.url.split('/')[8].split('.')[0];

      if (imageD.sort[0] === '0') {
        imageD.sort = imageD.sort.slice(1);
      }

      advert.images.push(imageD);
    }

    return advert;
  }

  parseInfoHome(offer){
    const infoHome = new InfoHome();
    infoHome.buildingName = offer['building-name'][0];
    infoHome.yandexBuildingId = offer['yandex-building-id'][0];
    infoHome.buildingState = offer['building-state'][0];
    infoHome.builtYear = offer['built-year'][0];
    infoHome.readyQuarter = offer['ready-quarter'][0];
    infoHome.buildingSection = offer['building-section'][0];

    infoHome.images = [];
    for (const image of offer.image) {
      if (typeof image !== 'string') continue;

      const imageD = new Image();
      imageD.dow = false;
      imageD.file = '';

      imageD.url = image;

      imageD.sort = imageD.url.split('/')[8].split('.')[0];

      if (imageD.sort[0] === '0') {
        imageD.sort = imageD.sort.slice(1);
      }

      infoHome.images.push(imageD);
    }

    return infoHome;
  }

  async getDoc() {
    const xml2js = require('xml2js');
    const parser = new xml2js.Parser();

    parser.on('error', err => {
      console.log('Parser error', err);
    });

    const result =  await new Promise(async end => {
      http.get('http://feeds.l-invest.ru/yandex/yandex.xml', res => {
        if (res.statusCode >= 200 && res.statusCode < 400) {
          let data = '';
          res.on('data', data_ => {
            data += data_.toString();
          });
          res.on('end', () => {
            parser.parseString(data, (err, result_) => {
              end(result_);
            });
          });
        }
      });
    });

    return JSON.parse(JSON.stringify(result['realty-feed'].offer));
  }
}
