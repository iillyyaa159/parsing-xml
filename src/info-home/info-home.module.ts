import { Module } from '@nestjs/common';
import { InfoHomeService } from './info-home.service';
import { InfoHome } from './info-home.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([InfoHome])],
  providers: [InfoHomeService],
})
export class InfoHomeModule {}
