import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InfoHome } from './info-home.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class InfoHomeService {
  constructor(
    @InjectRepository(InfoHome)
    private readonly infoHomeRepository: Repository<InfoHome>,
  ) {}

  async findOne(infoHome) {
    return await this.infoHomeRepository.findOne({
      where: {
        yandexBuildingId: infoHome.yandexBuildingId,
      },
    });
  }

  async insert(infoHome) {
    return await this.infoHomeRepository.insert(infoHome);
  }
}
