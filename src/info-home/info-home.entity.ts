import { Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn, PrimaryColumn } from 'typeorm';
import { Advert } from '../advert/advert.entity';
import { Image } from '../image/image.entity';

@Entity()
export class InfoHome {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  buildingName: string;

  @Column()
  yandexBuildingId: number;

  @Column()
  buildingState: string;

  @Column()
  builtYear: number;

  @Column()
  readyQuarter: number;

  @Column()
  buildingSection: string;

  @Column({ nullable: true })
  main_photo_id: number;

  @OneToMany(type => Advert, advert => advert.infoHome, {
    cascade: true,
  })
  adverts: Advert[];

  @OneToMany(type => Image, image => image.infoHome, {
    cascade: true,
  })
  images: Image[];
}
